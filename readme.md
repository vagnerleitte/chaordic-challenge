# Chaordic Full-stack Challenge (Vagner Leitte) 

Esse documento contém informações de como instalar e executar a aplicação em um servidor web.

O desafio foi desenvolvido na linguagem PHP utilizando o framework Laravel na versão 5.4.
Ele tem como dependências principais o uso da versão 5.6.4 do PHP ou superior e para esse desafio foi utilizado o banco de dados MySql.

## Instalação

Foi criado um arquivo [install.sh](https://bitbucket.org/vagnerleitte/chaordic-challenge/raw/master/install.sh "Script de instalação do APP") para automatizar o processo de configuração do ambiente de execução da aplicaçao.
O script foi desenvolvido e testado para a versão 14.04 LTS do Ubuntu server ou superior em uma instalação limpa baseada em uma instância Amazom EC2 com a imagem do Ubuntu 14.04 LTS fornecida pela amazom.

Para iniciar a instalação da aplicação no servidor de forma automatizada você deve baixar o arquivo [install.sh](https://bitbucket.org/vagnerleitte/chaordic-challenge/raw/master/install.sh "Script de instalação do APP") no seu servidor
definir as permissões corretas de execução e executar o script.

Para isso, devemos fazer login na instância do servidor que irá servir a aplicação via SSH.
Após conectado, seguimos os seguintes passos.

```
~$ wget https://bitbucket.org/vagnerleitte/chaordic-challenge/raw/master/install.sh
~$ chmod +x install.sh
~$ ./install.sh
```

O script acima irá fazer o download e instalação do PHP na versão 7.0, MySql Server, Composer, Apache 2 e as bibliotecas de dependencia desses softwares.
**Por favor se atente a instalação pois será necessário confirmar algumas informações durante a instalação como a adição de novas entradas no registro PPA do ubuntu para instalação do PHP 7 e senha de acesso ao MySQL.**

Após a instalação o script irá gerar e/ou alterar os arquivos de configuração do Apache para apontar para o diretório criado para o app dentro de /var/www, ativar os
módulos e extensões necessárias e reiniciar o servidor Apache.

Em seguida será feito o clone do repositório do desafio dentro do diretório criado pelo script e a instalação das dependências do Laravel via composer, alem da criação de um arquivo de configuração de ambiente chamado .env 
que será o responsável por amazenar as informações para o funcionamento correto da aplicação.

## Configurando a Aplicação

Antes de executarmos a aplicação precisamos fazer algumas alterações nesse arquivo. Caso ele não tenha sido criado, você pode criá-lo ou copiá-lo do arquivo de exemplo que é comitado junto com o Laravel.


### Criando um novo arquivo

```
touch .env
```

### Copiando o arquivo de exemplo

```
cp .env.example .env
```

Após criarmos o arquivo, precisamos definir algumas configurações nele. Esse documento presume que você fez uma cópia do arquivo de exemplo.
O conteúdo do arquivo de exemplo deverá ser igual ou semelhante ao trecho abaixo.

```
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=

```

Para o nosso teste precisamos nos preocupar apenas com as seguintes linhas.

```

APP_KEY=


APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

Para gerar uma nova chave você pode usar o comando key:generate do artisan.

```
php artisan key:generate
```

Em APP_URL você deve definir a url de domínio do servidor. Como utilizado no exemplo publicado no AWS.

```
APP_URL=http://ec2-54-94-211-124.sa-east-1.compute.amazonaws.com/
```

Em uma aplicação convencional seria algo semelhante a linha abaixo
Supondo que o domínio do nosso site fosse short.co

```
APP_URL=http://short.co/
```

A última configuração a ser feita em nosso exemplo é do banco de dados.
O Laravel suporta os seguintes banco de dados nativamente.
- MySQL
- Postgres
- SQLite
- SQL Server

Para esse exemplo foi testado e validado o funcionamento no MySQL.

```
DB_CONNECTION=mysql     #driver de conexão. Pode ser MySql, SQL Server, SQLite ou Postgres
DB_HOST=127.0.0.1       #endereço do servidor de banco de dados
DB_PORT=3306            #porta do servidor
DB_DATABASE=homestead   #nome do banco de dados
DB_USERNAME=homestead   #usuário do banco de dados
DB_PASSWORD=secret      #senha do banco de dados
```

Após essas configurações, podemos criar nossa estrutura de banco de dados para armazenar as informações da aplicação.

Lembrando que o banco de dados deve ter sido criado antes de executar as migrações de banco de dados.

Para criar o banco de dados no MySQL podemos seguir os passos abaixo.

Acesse o servidor de dados via SSH. Isso pode ser feito também usando o PHPMyadmin caso esteja instalado no servidor.

Acesse o console do MySQL

```
mysql -uNOME_DO_USUARIO -p
```

Em seguida será solicitado a senha desse usuário. Caso o usuário tenha sido configurado com senha em branco o console do MySQL já estará disponível.

Digite o comando de criação de banco de dados.

```
mysql> CREATE DATABASE nome_do_banco
```

O nome do banco de dados deve ser o mesmo definido em DB_DATABASE no arquivo .env.Digite exit para sair do console do mysql.

Em seguida voltamos ao console da instância de servidor da aplicação para finalizarmos a instalação da aplicação.

Vamos navegar até o diretório da aplicação que por padrão é chaordic-challenge e está localizado em /var/www

```
~$ cd /var/www/chaordic-challenge
~$ php artisan migrate
```

Nesse ponto, a aplicação deve estar disponível para ser acessada. Essa documentação não cobre a criação e configuração de uma instância EC2, apenas presumimos que essa etapa já tenha sido concluída anteriormente.

Foram criadas rotinas de teste para todos os endpoints solicitados e pode ser visualizado utilizando o PHPUnit como no exemplo abaixo. 
Você também pode utilizar o parâmetro --coverage-html=/var/www/chaordic-challenge/public/coverage para gerar os relatórios de cobertura de testes
do PHPUnit. Nesse desafio, me concentrei nos testes nos controllers da aplicação por conta do escopo e tempo. 

**_Para gerar os relatórios de cobertura de código, o diretório de destino deve existir e preferencialmente ter as permissões definidas para 0777._**

```
~$ ./vendor/bin/phpunit --coverage-html=/var/www/chaordic-challenge/public/coverage
PHPUnit 6.0.6 by Sebastian Bergmann and contributors.

...................                                               19 / 19 (100%)

Time: 4.05 seconds, Memory: 18.00MB

OK (19 tests, 39 assertions)

Generating code coverage report in HTML format ... done
```

**_Ao rodar os testes alguns dados (dummy data) serão criados no banco de dados._**

Tudo pronto.