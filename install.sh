#!/bin/sh

NoColor='\033[0m'       

Vermelho='\033[0;31m'
Verde='\033[0;32m'
Amarelo='\033[0;33m'
Roxo='\033[0;35m'
Ciano='\033[0;36m'
SITE="chaordic-challenge"
# Limpando instalações antigas.
echo "$Ciano \n Limpando instalações antigas.. $NoColor"

if [ -d /var/www/$SITE ]; then
        sudo rm -rf /var/www/$SITE
fi

# Atualizando pacotes do sistema.
echo "$Ciano \n Atualizando sistema.. $NoColor"
sudo apt-get update -y && sudo apt-get upgrade -y

## Instalando Apache2, Mysql e PHP 
echo "$Ciano \n Instalando Apache2 $NoColor"
sudo apt-get install apache2 apache2-doc apache2-mpm-prefork apache2-utils libexpat1 ssl-cert -y

echo "$Ciano \n Instalando MySQL $NoColor"
sudo apt-get install mysql-server mysql-client libmysqlclient15.dev -y

echo "$Ciano \n Instalando PHP & Bibliotecas necessárias $NoColor"
echo "$Amarelo \n Adicionando repositório do PHP 7 no PPA $NoColor"
sudo apt-get install -y language-pack-en-base
sudo LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php -y

echo "$Amarelo \n Instalando dependencia recomendada Phyton Sofwrare Properties.. $NoColor"
sudo apt-get install python-software-properties
sudo apt-get install software-properties-common

echo "$Amarelo \n Atualizando sistema.. $NoColor"
sudo apt-get update

echo "$Amarelo \n Instalando PHP 7 $NoColor"
sudo apt-get install php7.0 php7.0-mysql php7.0-mbstring php7.0-dom php7.0-xml php7.0-curl -y

echo "$Ciano \n Verificar Instalações $NoColor"
sudo apt-get install apache2 php7.0 mysql-server php7.0-mysql mysql-client mysql-server -y

echo "$Ciano \n Habilitando extensões requeridas pelo laravel. $NoColor"
sudo phpenmod mbstring dom mcrypt

# Hablitar mod_rewrite, necessário para o correto funcionamento do laravel
echo "$Ciano \n Habilitando Módulos $NoColor"
sudo a2enmod rewrite

# Versão do PHP
echo "$Roxo \n Verificando versão do PHP $NoColor"
php -v
echo "$Roxo \n PHP $NoColor"

# Instalando Git
echo "$Ciano \n Instalando Git $NoColor"
sudo apt-get install git -y

## Configurando
# Permissões de usuário e diretório
echo "$Ciano \n Permissões for /var/www $NoColor"
sudo chown -R www-data:www-data /var/www
echo "$Green \n Permissões alteradas $NoColor"

# Criar diretório /var/www/chaordic-challenge
echo "$Ciano \n Criando diretório $SITE $NoColor"

if [ ! -d /var/www/$SITE ]; then
        sudo mkdir /var/www/$SITE
fi

# Permissoes da pasta

echo "$Ciano \n Alterando permissões da pasta /var/www/$SITE para 0755 $NoColor"
sudo chmod 0755 /var/www/$SITE

echo "$Ciano \n Instalando Compser $NoColor"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '55d6ead61b29c7bdee5cccfb50076874187bd9f21f65d8991d46ec5cc90518f447387fb9f76ebae1fbbacf329e583e30') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/bin --filename=composer
php -r "unlink('composer-setup.php');"

# Ir para o diretório /var/www/chaordic-challenge e instalar o app

sudo sed -i "s_DocumentRoot /var/www/html_DocumentRoot /var/www/$SITE/public_" /etc/apache2/sites-available/000-default.conf


if grep -Fxq "<Directory /var/www/$SITE/>" my_list.txt
then
    echo "$Amarelo \n Configurações de diretório para mod_rewrite já aplicadas. $NoColor"
else
    sudo sed -i "/\/VirtualHost/i \        <Directory\ \/var\/www\/$SITE\/\>\n           AllowOverride All\n\        <\/Directory\>" /etc/apache2/sites-available/000-default.conf
fi

echo "$Ciano \n Navegando para diretório /var/www/$SITE $NoColor"
cd "/var/www/$SITE"

echo "$Ciano \n Clonando diretório da aplicação /var/www/$SITE $NoColor"
sudo git clone https://vagnerleitte@bitbucket.org/vagnerleitte/chaordic-challenge.git ./

echo "$Ciano \n Sincronizando diretório da aplicação com o repositório master $NoColor"
sudo git pull origin master

# Permissões de usuário e diretório nos arquivos do app
echo "$Ciano \n Permissões for /var/www/$SITE $NoColor"
sudo chown -R www-data:www-data "/var/www/$SITE"
echo "$Green \n Permissões alteradas $NoColor"

#Instalando dependencias do composer
echo "$Green \n Instalando dependencias do composer na aplicação $NoColor"


if [ ! -f /var/www/$SITE/.env ]; then
        echo "$Ciano \n Criando arquivo de configuração de ambiente da aplicação em /var/www/$SITE/.env $NoColor"
        sudo cp /var/www/$SITE/.env.example /var/www/$SITE/.env
fi

sudo composer update -vvv

echo "$Ciano \n Gerando chave de aplicação $NoColor"
sudo php artisan key:generate

# Definindo permissões de usuário e diretório da aplicação
echo "$Ciano \n Permissões for /var/www/$SITE $NoColor"
sudo chown -R www-data:www-data "/var/www/$SITE"
echo "$Green \n Permissões alteradas $NoColor"

# Reiniciar o Apache2
echo "$Ciano \n Reiniciando Apache2 $NoColor"
sudo service apache2 restart
