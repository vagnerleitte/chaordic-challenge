<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    protected static $id;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$id = str_random(20);
    }

    /**
     * Criando novo usuário
     * Deve criar um novo usuário no banco de dados caso ele não exista.
     *
     * @return void
     */
    public function testCreateUser()
    {

        $response = $this->json('POST', '/api/users', ['id' => self::$id]);

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'id' => self::$id,
            ]);
    }
    /**
     * Tentando criar um novo usuário com um id já existente no banco de dados
     * Deve criar retornar o código HTTP 409 (CONFLICT)
     * Deve retornar um array vazio
     *
     * @return void
     */
    public function testCreateUserWithExistentId()
    {

        $response = $this->json('POST', '/api/users', ['id' => self::$id]);

        $response
            ->assertStatus(409)
            ->assertExactJson([]);
    }

    /**
     * Verifica validação de input de dados
     * Deve criar retornar o código HTTP 400 (BAD REQUEST)
     * Deve retornar o erro de validação "The id field is required."
     *
     * @return void
     */
    public function testValidateCreateUserFiels()
    {
        $id = str_random(100);

         $response = $this->json('POST', '/api/users', ['name' => $id]);

        $response
            ->assertStatus(400)
            ->assertExactJson([
                "id" => [
                    "The id field is required."
                ]
            ]);
    }

    /**
     * Exclui um usuário do banco de dados
     * Deve criar retornar o código HTTP 200 (OK)
     *
     * @return void
     */
    public function testDeleteUser()
    {
        $response = $this->delete("/api/user/" .self::$id);
        $response->assertStatus(200);
    }

    /**
     * Tenta excluir um usuário não existente do banco de dados
     * Deve criar retornar o código HTTP 404 (Not Found)
     * Deve retornar um json com a mensagem "User not found."
     *
     * @return void
     */
    public function testDeleteNonExistentUser()
    {
        $response = $this->delete("/api/user/" .self::$id);
        $response->assertStatus(404)
        ->assertExactJson([
                "error" =>"User not found."
            ]);
    }
}
