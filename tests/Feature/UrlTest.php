<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Url;
use Faker\Factory as Faker;

class UrlTest extends TestCase
{
    protected static $user;
    
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        
    }

    /**
     * Verifica a validação dos campos de url
     * Deve retornar o código de erro 400 com a mensagem "The url field is required."
     *
     * @return void
     */
    public function testValidateCreateUrlFiels()
    {
        $user = factory(User::class)->make();    
        
        $response = $this->json('POST', "/api/users/{$user->name}/urls", []);

        $response
            ->assertStatus(400)
            ->assertExactJson([
                "url" => [
                    "The url field is required."
                ]
            ]);
    }

    /**
     * Tenta cadastrar uma url com um usuário não existente.
     * Deve retornar o código de erro 400 com a mensagem "User not found."
     *
     * @return void
     */
    public function testCreateUrlWithNonExistentUser()
    {
        $faker = Faker::create();
        $user = str_random();
        
        $response = $this->json('POST', "/api/users/{$user}/urls", ['url' => $faker->url]);

        $response
            ->assertStatus(400)
            ->assertExactJson([
                "error" => "User not found."
            ]);
    }

    /**
     * Cria uma nova url para um usuário
     *
     * @return void
     */
    public function testCreateUrl()
    {
        $faker = Faker::create();
        $user = factory(User::class)->create();
        $url = $faker->url;
        
        $response = $this->json('POST', "/api/users/{$user->name}/urls", ['url' => $url]);

        $response
            ->assertStatus(201)
            ->assertJsonFragment([
                    "hits" => 0,
                    "url" => $url
            ]);
    }

    /**
     * Cria uma nova url para um usuário
     *
     * @return void
     */
    public function testOpenInvalidUrl()
    {

        $url = url('/') . '/' . str_random(30);
        
        $response = $this->json('GET', $url);
        
        $response
            ->assertStatus(404)
            ->assertJson([
                    "error" => "Url not found."
            ]);
    }

    /**
     * Cria uma nova url para um usuário
     *
     * @return void
     */
    public function testOpenValidUrl()
    {
        $faker = Faker::create();
        $url = factory(Url::class)->create();
        
        $response = $this->json('GET', $url->short_url);

        $response
            ->assertStatus(301)
            ->assertHeader('location', $url->url);
    }

    /**
     * Visualiza os detalhes de uma url inválida
     * Deve retornar o código de erro 400 com a mensagem "Url not found."
     *
     * @return void
     */
    public function testGetStatsForInvalidUrl()
    {
        $url = url('/api/stats') . '/' . str_random(30);
        
        $response = $this->json('GET', $url);
        
        $response
            ->assertStatus(404)
            ->assertJson([
                    "error" => "Url not found."
            ]);
    }

    /**
     * Visualiza os detalhes de uma url válida
     * Deve retornar um json com os detalhes de uma url
     *
     * @return void
     */
    public function testGetStatsForValidUrl()
    {
        $url = factory(Url::class)->create();
        
        $id = str_replace(url('/') .'/', '', $url->short_url);
        
        $response = $this->json('GET', "/api/stats/{$id}");

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                    "id" => $url->id,
                    "hits" => (int) $url->hits,
                    "url" => $url->url,
                    "shortUrl" => $url->short_url
            ]);
    }


    /**
     * Visualiza os detalhes de das urls do banco de dados
     * Deve retornar um json com os detalhes das urls no banco de dados.
     *
     * @return void
     */
    public function testGetStatsForAllUrl()
    {
        $response = $this->json('GET', "/api/stats/");

        $string = $response
                    ->assertStatus(200)
                    ->getContent();
        
        $this->assertTrue(is_string($string) && is_array(json_decode($string, true)) ? true : false);
    }

    
    /**
     * Visualiza os detalhes de das urls do banco de dados para um usuário não existente
     * Deve retornar um json com o código de erro 404 e a mensagem de erro "User not found."
     *
     * @return void
     */
    public function testGetStatsForAllUrlByUser()
    {

        $user = factory(User::class)->create();

        $urls = factory(Url::class, 10)->create([
            'user_id' => $user->id
        ]);

        $response = $this->json('GET', "/api/users/{$user->name}/stats");

        $string = $response
                    ->assertStatus(200)
                    ->getContent();
        
        $this->assertTrue(is_string($string) && is_array(json_decode($string, true)) ? true : false);
    }

    /**
     * Visualiza os detalhes de das urls do banco de dados para um usuário
     * Deve retornar um json com os detalhes das urls no banco de dados de um usuário.
     *
     * @return void
     */
    public function testGetStatsForAllUrlByUserNotFound()
    {
        $response = $this->json('GET', "/api/users/notexits/stats");

        $response
            ->assertStatus(404)
                ->assertJson([
                    "error" => "User not found."
            ]); 
    }

   /**
     * Visualiza os detalhes de das urls do banco de dados para um usuário
     * Deve retornar um json com os detalhes das urls no banco de dados de um usuário.
     *
     * @return void
     */
    public function testDeleteUrlById()
    {
        $user = factory(User::class)->create();

        $url = factory(Url::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->delete("/api/urls/" . $url->id);
        $response->assertStatus(200);
    }

   /**
     * Visualiza os detalhes de das urls do banco de dados para um usuário
     * Deve retornar um json com os detalhes das urls no banco de dados de um usuário.
     *
     * @return void
     */
    public function testDeleteNonExistentUrlById()
    {
        $response = $this->delete("/api/urls/" . $this->getRandonIdForNonExistentUrl());
        $response->assertStatus(404)
            ->assertJson([
                        "error" => "Url not found."
                ]);
    }

    protected function getRandonIdForNonExistentUrl(){
        $id = rand(1, getrandmax());

        $url = Url::find($id);
        
        if(!is_null($url))
            return $this->getRandonIdForNonExistentUrl();
        
        return $id;
    }

}
