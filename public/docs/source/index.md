---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost:8000/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_8f7d6bb7822b868c4fa0114e89d2cf97 -->
## Store a newly create url on database.

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users/{id}/urls" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{id}/urls",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/users/{id}/urls`


<!-- END_8f7d6bb7822b868c4fa0114e89d2cf97 -->

<!-- START_c09271210f3ae5fb9301e7c604a52a3d -->
## Get system stats.

> Example request:

```bash
curl -X GET "http://localhost:8000/api/stats" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/stats",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "urlCount": 608,
    "hits": 17809156,
    "topUrls": [
        {
            "id": 141,
            "url": "http:\/\/stroman.info\/est-repudiandae-ducimus-reprehenderit-aut-fugiat-vel-earum",
            "short_url": "http:\/\/localhost:8000\/oQTpIAi",
            "hits": 9658743
        },
        {
            "id": 139,
            "url": "http:\/\/www.gutmann.net\/",
            "short_url": "http:\/\/localhost:8000\/1AAXbI8",
            "hits": 6958325
        },
        {
            "id": 138,
            "url": "http:\/\/grimes.com\/illo-aut-commodi-qui-velit-repudiandae-tempore-ut",
            "short_url": "http:\/\/localhost:8000\/cR5xX8a",
            "hits": 968575
        },
        {
            "id": 136,
            "url": "https:\/\/kris.info\/omnis-perspiciatis-voluptatibus-quia-error-recusandae-quos-qui.html",
            "short_url": "http:\/\/localhost:8000\/KVx19dD",
            "hits": 102548
        },
        {
            "id": 134,
            "url": "http:\/\/brown.biz\/",
            "short_url": "http:\/\/localhost:8000\/9bulM0z",
            "hits": 59681
        },
        {
            "id": 140,
            "url": "http:\/\/www.osinski.biz\/quam-sit-laborum-dolorum-est-maiores-beatae-autem",
            "short_url": "http:\/\/localhost:8000\/7d3VhwC",
            "hits": 45650
        },
        {
            "id": 133,
            "url": "http:\/\/hirthe.com\/quia-aut-et-id-delectus-et-nihil.html",
            "short_url": "http:\/\/localhost:8000\/LymZxnC",
            "hits": 7065
        },
        {
            "id": 137,
            "url": "http:\/\/douglas.com\/quae-consectetur-et-totam-eum.html",
            "short_url": "http:\/\/localhost:8000\/LlehvpN",
            "hits": 6959
        },
        {
            "id": 135,
            "url": "http:\/\/mitchell.com\/",
            "short_url": "http:\/\/localhost:8000\/JpB43my",
            "hits": 1587
        },
        {
            "id": 273,
            "url": "http:\/\/www.outlook.com",
            "short_url": "http:\/\/localhost:8000\/5Qav9hY",
            "hits": 14
        }
    ]
}
```

### HTTP Request
`GET api/stats`

`HEAD api/stats`


<!-- END_c09271210f3ae5fb9301e7c604a52a3d -->

<!-- START_1c5922d3e79c56293cc5d25218815d04 -->
## Get url stats for user if exists

> Example request:

```bash
curl -X GET "http://localhost:8000/api/users/{id}/stats" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{id}/stats",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "User not found."
}
```

### HTTP Request
`GET api/users/{id}/stats`

`HEAD api/users/{id}/stats`


<!-- END_1c5922d3e79c56293cc5d25218815d04 -->

<!-- START_dc29ba7c17136fb3b84bfc69c0e6016d -->
## Get url stats if exists

> Example request:

```bash
curl -X GET "http://localhost:8000/api/stats/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/stats/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Url not found."
}
```

### HTTP Request
`GET api/stats/{id}`

`HEAD api/stats/{id}`


<!-- END_dc29ba7c17136fb3b84bfc69c0e6016d -->

<!-- START_12e37982cc5398c7100e59625ebb5514 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/users`


<!-- END_12e37982cc5398c7100e59625ebb5514 -->

<!-- START_23c3af5ce87bc4853e9895cf97839669 -->
## Delete a user from database.

> Example request:

```bash
curl -X DELETE "http://localhost:8000/api/urls/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/urls/{id}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/urls/{id}`


<!-- END_23c3af5ce87bc4853e9895cf97839669 -->

<!-- START_a1ef15db35f08591deb485d3c5fb9a31 -->
## Delete a user from database.

> Example request:

```bash
curl -X DELETE "http://localhost:8000/api/user/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/user/{id}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/user/{id}`


<!-- END_a1ef15db35f08591deb485d3c5fb9a31 -->

