<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {

    return [
        'name' => str_random(20),
    ];
});

$factory->define(App\Url::class, function (Faker\Generator $faker) {
    
    return [
        'url' => $faker->url,
        'short_url' => url('/') . '/' . str_random(7),
        'user_id' => factory(App\User::class)->create()->id
    ];
});
