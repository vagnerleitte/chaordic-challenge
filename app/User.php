<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Url;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Get url collection for user
     * @param  String field name to order
     * @param  String asc or desc
     * @return Collection of Urls
     */

    public function urls($order = 'id', $direction = 'asc')
    {
        return $this->hasMany(Url::class)->orderBy($order, $direction);
    }
}
