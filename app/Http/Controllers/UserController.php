<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Input;
use Response;
use Validator;

class UserController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $data = $request->all();
    
        if (User::where('name', '=', $data['id'])->exists()) {
            return Response::json(null, 409);
        }

        $user = User::create(['name' => $data['id']]);

        $response = [];

        if($user){
            $response = ['id' => $user->name];
        }
        
        return Response::json($response, 201);

    }

    /**
     * Delete a user from database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = User::where('name', $id);

        if($user->count() == 0){
            return Response::json(["error" => "User not found."], 404);
        }

        if(!$user->delete()){
            return Response::json(["error" => "The server has crashed."], 500);
        }

        return response("", 200);
    }
}
