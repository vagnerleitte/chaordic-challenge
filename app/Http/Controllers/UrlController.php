<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Url;
use App\User;
use Validator;
use Response;

class UrlController extends Controller
{
    /**
     * Store a newly create url on database.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'url' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $user = User::where('name', '=', $id);
        if (!$user->exists()) {
            return Response::json(["error" => "User not found."], 400);
        }

        $data = $request->all();

        $shortUrl = $this->generateUrl();
        
        $url = Url::create([
            'url' => $data['url'],
            'short_url' => $shortUrl,
            'user_id' => $user->first()->id
        ]);

        if(!$url){
            return Response::json(["error" => "Server has crashed."], 500);
        }
        
        $response = [
            'id' => $url->id,
            'url' => $url->url,
            'shortUrl' => $url->short_url,
            'hits' => (int) $url->hits,
            ];
        
        return Response::json($response, 201);
     }

    /**
     * Generate a new short url and verify if not exists in database
     * @return String url
     */
     protected function generateUrl()
     {
        $shortUrl = url('/') . '/' . str_random(7);

        if(Url::where('short_url', '=', $shortUrl)->exists())
            return $this->generateUrl();
        
        return $shortUrl;
     }

    /**
     * Redirect to a url if exists
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response || \Illuminate\Support\Facades\Redirect
     */
     public function go(Request $request)
     {
        $shortUrl = $request->fullUrl();

        $url = Url::where('short_url', '=', $shortUrl)->first();

        if(!is_null($url)){
            $url->update([
                'hits' => ++$url->hits
            ]);

            return redirect($url->url, 301);
        }

        return Response::json(['error' => 'Url not found.'], 404);
     }

     /**
     * Get url stats if exists
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function urlStats($id)
     {
        $shortUrl = url('/') . '/' . $id;

        $url = Url::where('short_url', '=', $shortUrl)->first();

        if(!is_null($url)){
            
            $data = [
                "id" => $url->id,
                "hits" => (int) $url->hits,
                "url" => $url->url,
                "shortUrl" => $url->short_url
            ];

            return Response::json($data, 200);
        }

        return Response::json(['error' => 'Url not found.'], 404);
     }

    /**
     * Get system stats.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function stats()
    {
        $data = [];

        $data['urlCount'] = Url::all()->count();
        $data['hits'] = Url::all()->sum('hits');
        $data['topUrls'] = Url::take(10)->orderBy('hits', 'desc')->get();

        return Response::json($data);
    }
    
    /**
     * Get url stats for user if exists
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function userStats($id)
    {

        $user = User::where('name', '=', $id)->first();

        if(is_null($user)){
            return Response::json(["error" => "User not found."], 404);
        }
        
        $data = [];

        $url = $user->urls('hits', 'desc');

        $data['urlCount'] = $url->count();
        $data['userId'] = $user->name;
        $data['hits'] = $url->sum('hits');
        $data['topUrls'] = $url->get();

        return Response::json($data);
    }

    /**
     * Delete a user from database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function delete($id)
    {
        $url = Url::find($id);

        if(is_null($url)){
            return Response::json(["error" => "Url not found."], 404);
        }

        if(!$url->delete()){
            return Response::json(["error" => "The server has crashed."], 500);
        }

        return response("", 200);
    }
}
