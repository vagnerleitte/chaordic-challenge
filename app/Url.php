<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Url extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'short_url', 'user_id', 'hits'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'created_at', 'updated_at'
    ];

    /**
     * Get url stats if exists
     * @return Collection
     */

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
